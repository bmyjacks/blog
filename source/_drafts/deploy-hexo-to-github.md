---
title: 如何上传Hexo到GitHub上，并使用Netlify进行部署
tags:
    - hexo
    - github
    - git
categorites :
    - hexi
    - github
---
## 首先，注册一个GitHub和Netlify账号
{% note info %}
### 注意
在此不演示如何注册GitHub和Netlify账号
{% endnote %}

## 初始化hexo安装文件夹
进入文件夹，输入`git init`初始化git仓库